/*
ACTIVITY 20

1. In the s20 folder, create an a1 folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.


13. Create a git repository named s20.
14. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
15. Add the link in Boodle.

*/

console.log ("Activity 20")
// PROBLEM 1

/*
	1. Create a variable number that will store the value of the number provided by the user via the prompt.
	2. Create a for loop that will be initialized with the number provided by the user, will stop when the value is less than or equal to 0 and will decrease by 1 every iteration.
	3. Create a condition that if the current value is less than or equal to 50, stop the loop.
	4. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
	5. Create another condition that if the current value is divisible by 5, print the number.
*/


let x = Number(prompt ("Give me a number (preferably higher than 50) :")) ;

for (let count = x ; count >= 0 ; count --)
	{
	
		if (count % 10 === 0)
			{
			console.log ("This number is divisible by 10") ; 
			continue ;	
			}

		if (count % 5 === 0)
			{
			console.log (count)
			continue ;
			}

	//console.log ("Current count: " + count) ;

		if (count <= 50 )
			{
			console.log ("Number less than 50, stopping the loop") ;
			break ;
			}
	}


// PROBLEM 2

/*
	1. Create a variable that will contain the string supercalifragilisticexpialidocious.
	2. Create another variable that will store the consonants from the string.
	3. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
	4. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
	5. Create an else statement that will add the letter to the second variable.
*/

let word = "supercalifragilisticexpialidocious" ;
console.log (word)

let consonants = "" ;

	for (let i = 0 ; i < word.length ; i++)
	{
	if (
		word[i].toLowerCase() === "a" ||
		word[i].toLowerCase() === "e" ||
		word[i].toLowerCase() === "i" ||
		word[i].toLowerCase() === "o" ||
		word[i].toLowerCase() === "u" 
		)
		{
		continue ;
		}
	else
		{
		consonants = consonants + word[i]  ;
		}
	} ; 

console.log (consonants)

